#Helm and kubectl
update_aliases() {
  kubecfg="${HOME}/k8s/"
  # ls "${HOME}/k8s/*.config" | \
  for cfg in $(ls ${kubecfg}*_kube.config); do
    envname=$(basename ${cfg})
    envname=${envname%\_kube.config}
    echo ${envname}
    alias helm-${envname}="helm --kubeconfig ${cfg} \$*"
    alias kubectl-${envname}="kubectl --kubeconfig ${cfg} \$*"
  done
}

#vault-connect ENVNAME to connect
vault-connect() {
  envname=$1
  kubeconfig=~/k8s/${envname}_kube.config
  if ! [ -f $kubeconfig ]; then
    echo "kubeconfig: $kubeconfig does not exist"
    return 1
  fi
  export VAULT_TOKEN=$(kubectl --kubeconfig $kubeconfig -n pipeline get secret vaultseal -o jsonpath="{.data.unseal}" | \
    base64 -d | jq -r .root_token)
  export VAULT_SKIP_VERIFY=true
  export VAULT_ADDR=https://localhost:8201
  echo -e "\033[0;31m Connecting to ${envname} \033[0m"
  env | grep "VAULT_"
  kubectl --kubeconfig $kubeconfig -n pipeline port-forward deployment/vault 8201:8200 &
}

# alias_usage