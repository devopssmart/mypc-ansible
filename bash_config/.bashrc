#------------------------------------------------
if [[ $- == *i* ]]
then
    bind '"\e[A": history-search-backward'
    bind '"\e[B": history-search-forward'
fi

PROMPT_COMMAND='history -a'
shopt -s cmdhist

update_aliases
